## recycle points mapbox vector tiles server

### Run server
1. `npm install`
2. create .env file: `touch .env` with following params:
   ```
   PGUSER=postgres
   PGHOST=localhost
   PGPASSWORD=
   PGDATABASE=points
   PGPORT=5432
   ```
   you can change values according to your settings
3. start server: `node express.js`


### Usage

In mapbox-gl:

```js
map.addSource('points', {
  type: 'vector',
  tiles: ['http://localhost:3005/points/{z}/{x}/{y}/tile.mvt?fractions=1,2,4,5'],
});
```

or in react-map-gl:
```tsx
<Source
    id="points"
    type="vector"
    tiles={['http://localhost:3005/points/{z}/{x}/{y}/tile.mvt?fractions=1,2,4,5']}
></>
``` 


#### heroku stuff

    git push heroku master

    heroku open

    heroku logs --tail

    heroku local web
    
    heroku addons
    
    heroku run bash
    
    // env
    heroku config:set TIMES=2
    
    heroku config
    
    heroku pg:psql
    
    heroku restart
    