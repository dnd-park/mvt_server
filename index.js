require('dotenv').config();
const express = require('express');
const uuid = require('uuid');

const PORT = process.env.PORT || 5000

const table = 'points';
const geometry = 'coordinates';
const maxZoomLevel = 20;

// const setup = require('./setup');

const { TileServer } = require('clusterbuster-with-arrray');
TileServer({
	maxZoomLevel,
	attributes: ['id', 'fractions', 'title'],
	arrayAttributes: ['fractions'],
	filtersToWhere: (filters = { fractions: undefined, operator: undefined }) => {
		// You are responsible for protecting against SQL injection in this function. Because there are many ways to filter, it depends on the filter type on how to approach this.


		let operator = '@>';
		if (filters.operator) {
			operator = filters.operator === 'contains' ? '@>' : '&&';
		}

		const whereStatements = [];
		if (filters.fractions) {
			whereStatements.push(`fractions ${operator} ARRAY[${filters.fractions}]`);
		}

		whereStatements.push(`status='PUBLISHED'`);

		return whereStatements;
	},
	pgPoolOptions: {
		// connectionString: process.env.PGURI,
		connectionString: process.env.DATABASE_URL,
	},
	cacheOptions: {
		enabled: true,
		lruOptions: {
			// что-то не работает
			max: 30  * 100
		}
	},
}).then(server => {
	const app = express();
	app.use((_, res, next) => {
		res.header('Access-Control-Allow-Origin', '*');
		next();
	});
	app.get('/health', (_, res) => {
		res.status(200).send('OK');
	});
	app.get('/points/:z/:x/:y/tile.mvt.pbf', (req, res) => {
		req.id = uuid.v4();
		console.time(req.id);
		console.log('req.query', req.query);
		server({
			z: req.params.z,
			x: req.params.x,
			y: req.params.y,
			queryParams: req.query,
			extent: 4096,
			bufferSize: 256,
			table,
			geometry,
			id: req.id,
			sourceLayer: 'points',
			radius: 15,
			cacheTtl: 60 * 100,
		})
			.then(result => {
				res.setHeader('Content-Type', 'application/x-protobuf');
				res.setHeader('Content-Encoding', 'gzip');
				console.time('send ' + req.id);
				res.status(200).send(result);
				console.timeEnd('send ' + req.id);
				console.timeEnd(req.id);
			})
			.catch(e => {
				res.status(400).send('Oops');
			});
	});
	app.listen(PORT, () => console.log(`Recycle Points MVT Server app listening on port ${PORT}!`));
});
